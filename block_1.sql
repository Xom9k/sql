/*
9. Описание коллекций и экспонатов музея. Включает в себя: выставочные залы,
коллекции, экспонаты, стоимости билетов. Выставочные залы описываются
названием. Коллекции состоят из: названия, краткого описания, списка
выставочных залов, в которых они размещены, периода показа, определяющегося
датой начала и конца. Экспонаты состоят из: названия, краткого описания,
страховой стоимости, века создания, указания коллекции, в которую экспонат
входит, габаритных размеров, включающих высоту, ширину, длину, и условий
содержания, определяемых, необходимостью контроля температуры,
необходимостью контроля влажности, защищённостью от людей.
a. Стоимости билетов состоят из: типа посетителя 13, набора коллекций, к
которым по данному билету возможен доступ, стоимости в рублях.
*/

CREATE TABLE `Halls` (
 `Name` varchar(20) NOT NULL,
 PRIMARY KEY (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `Halls` (`Name`) VALUES ('First'), ('Second'), ('Third');

CREATE TABLE `Collection_V2` (
 `Name` varchar(20) NOT NULL,
 `Short_Brief` text NOT NULL,
 `Start` date NOT NULL,
 `End` date NOT NULL,
 PRIMARY KEY (`Name`),
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `Collection_V2` (`Name`, `Short_Brief`, `Start`, `End`) VALUES ('Col_1', 'Supa', '2019-11-24', '2019-11-30'), ('Col_2', 'Supa', '2019-11-24', '2019-11-30'), ('Col_3', 'Supa', '2019-11-24', '2019-11-30'), ('Col_4', 'qwe', '2019-12-03', '2019-12-17');

CREATE TABLE `Exhibits_V2` (
 `Name` varchar(20) NOT NULL,
 `Short_Brief` text NOT NULL,
 `Insurance` int(11) NOT NULL,
 `Centery` int(11) NOT NULL,
 `Collection` varchar(20) NOT NULL,
 `Height` int(11) NOT NULL,
 `Width` int(11) NOT NULL,
 `Length` int(11) NOT NULL,
 `Temperature` int(11) NOT NULL,
 `Humidity` int(11) NOT NULL,
 `Security` int(11) NOT NULL,
 KEY `Collection` (`Collection`),
 CONSTRAINT `Exhibits_V2_ibfk_1` FOREIGN KEY (`Collection`) REFERENCES `Collection_V2` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `Exhibits_V2` (`Name`, `Short_Brief`, `Insurance`, `Centery`, `Collection`, `Height`, `Width`, `Length`, `Temperature`, `Humidity`, `Security`) VALUES
('Ex_1', 'Super', 100000, 21, 'Col_1', 180, 30, 45, 36, 30, 8),
('Ex_2', 'cool', 150000, 20, 'Col_2', 90, 60, 90, 36, 35, 10),
('Ex_3', 'cool', 555555, 19, 'Col_3', 50, 50, 50, 50, 50, 5),
('Ex_4', 'Not suppa', 250000, 12, 'Col_1', 130, 35, 44, 36, 10, 0),
('Ex_5', 'qwe', 200000, 21, 'Col_1', 123, 23, 32, 36, 3, 5),
('Ex_6', '123', 2000, 13, 'Col_1', 123, 32, 12, 32, 32, 10),
('Ex_7', 'qwe', 200000, 21, 'Col_4', 123, 23, 32, 12, 32, 1),
('Ex_8', 'qwe', 200000, 21, 'Col_4', 123, 23, 32, 12, 32, 1),
('Ex_9', 'qwe', 200000, 21, 'Col_4', 123, 23, 32, 12, 32, 1),
('Ex_10', 'qwe', 200000, 21, 'Col_4', 123, 23, 32, 12, 32, 1);



CREATE TABLE `Tickets` (
 `ID` int(11) NOT NULL AUTO_INCREMENT,
 `Type` varchar(20) NOT NULL,
 `Price` int(11) NOT NULL,
 PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

INSERT INTO `Tickets` (`ID`, `Type`, `Price`) VALUES (NULL, 'Adult', '2000'), (NULL, 'Child', '1000'), (NULL, 'Adult', '3000'), (NULL, 'Adult', '4500');

CREATE TABLE `Tickets_Coll` (
 `ID` int(11) NOT NULL,
 `Coll_Name` varchar(20) NOT NULL,
 KEY `Tickets_Coll_ibfk_2` (`Coll_Name`),
 KEY `ID` (`ID`),
 CONSTRAINT `Tickets_Coll_ibfk_2` FOREIGN KEY (`Coll_Name`) REFERENCES `Collection_V2` (`Name`),
 CONSTRAINT `Tickets_Coll_ibfk_3` FOREIGN KEY (`ID`) REFERENCES `Tickets` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `Tickets_Coll` (`ID`, `Coll_Name`) VALUES
(1, 'Col_1'),
(2, 'Col_2'),
(4, 'Col_1'),
(4, 'Col_2'),
(10, 'Col_1'),
(10, 'Col_2'),
(10, 'Col_3');


/*
Исправление ошибок
*/


CREATE TABLE `Halls_Colls` (
 `Hall` varchar(20) NOT NULL,
 `Collection` varchar(20) NOT NULL,
 KEY `Hall` (`Hall`),
 KEY `Collection` (`Collection`),
 CONSTRAINT `Halls_Colls_ibfk_1` FOREIGN KEY (`Hall`) REFERENCES `Halls` (`Name`),
 CONSTRAINT `Halls_Colls_ibfk_2` FOREIGN KEY (`Collection`) REFERENCES `Collection_V2` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `Halls_Colls` (`Hall`, `Collection`) VALUES
('First', 'Col_1'),
('First', 'Col_4'),
('Second', 'Col_2'),
('Third', 'Col_3');