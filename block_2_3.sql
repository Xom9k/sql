/*
27.Выберите для каждого зала, количество выставленных в нём коллекций,
содержащих более трёх экспонатов.
*/

/* До изменения таблицы
CREATE TEMPORARY TABLE tmp ( Hall VARCHAR(255) NOT NULL, Coll VARCHAR(255) NOT NULL , cout INT(255) NOT NULL);
INSERT INTO tmp
SELECT Halls.Name, Collection_V2.Name, SUM(CASE WHEN Exhibits_V2.Name IS Null THEN 0 ELSE 1 END) as Summa FROM Halls, Collection_V2, Exhibits_V2 WHERE Collection_V2.Hall = Halls.Name AND Collection_V2.Name = Exhibits_V2.Collection GROUP BY Collection_V2.Name;

SELECT tmp.Hall, SUM(CASE WHEN tmp.cout > 3 THEN 1 ELSE 0 END) as Kol_vo FROM tmp GROUP BY tmp.Hall;

DROP TABLE tmp;
*/

CREATE TEMPORARY TABLE tmp ( Coll VARCHAR(255) NOT NULL , cout INT(255) NOT NULL);
INSERT INTO tmp
SELECT Collection_V2.Name, SUM(CASE WHEN Exhibits_V2.Name IS Null THEN 0 ELSE 1 END) as Summa FROM Collection_V2, Exhibits_V2 WHERE Collection_V2.Name = Exhibits_V2.Collection GROUP BY Collection_V2.Name;

SELECT Halls_Colls.Hall, SUM(tmp.cout > 3) as Kol_vo  FROM tmp, Halls_Colls WHERE Halls_Colls.Collection = tmp.Coll GROUP BY Halls_Colls.Hall;

DROP TABLE tmp;
